#!perl
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Ghobe::Getopt::Mooptions' ) || print "Bail out!\n";
}

diag( "Testing Ghobe::Getopt::Mooptions $Ghobe::Getopt::Mooptions::VERSION, Perl $], $^X" );
