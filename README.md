# NAME

Ghobe::Getopt::Mooptions - wrapper around [Getopt::Long](https://metacpan.org/pod/Getopt%3A%3ALong) returning parsed options as an object

# SYNOPSIS

    my ($opts, $args) = Ghobe::Getopt::Mooptions->parse([
        "length=i",
        "file=s",
        "verbose"
      ], @ARGV);
    
    if ($opts->verbose)
    {
      ...
    }
    do_something($opts->file, $opts->length);

# METHODS

## parse

    my ($opts, $args) = Ghobe::Getopt::Mooptions->parse(\@options_descriptions, @options);

`options_descriptions` - array reference containing options descriptions suitable for [Getopt::Long](https://metacpan.org/pod/Getopt%3A%3ALong).

`options` - list of options to be parsed (e.g. `@ARGV`)

Returns an options object and an array reference with all remaining parts from `@options`.

# AUTHOR

Christian Hesse, `<ghobe at ghobe.de>`

# SEE ALSO

[Getopt::Long](https://metacpan.org/pod/Getopt%3A%3ALong)

[MooX::Options](https://metacpan.org/pod/MooX%3A%3AOptions)
