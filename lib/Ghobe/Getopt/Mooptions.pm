package Ghobe::Getopt::Mooptions;
use strict;
use warnings;

use Getopt::Long ();
use Moo;
use namespace::clean;

use Ghobe::Getopt::Mooptions::Option;

our $VERSION = '0.03';

sub parse
{
    my $class = shift;
    my $options_descriptions = shift;
    my @args = @_;

    die "first parameter is not an array reference" unless ref $options_descriptions eq 'ARRAY';

    my $parser = Getopt::Long::Parser->new(
        'config' => [qw(no_auto_abbrev no_ignore_case)],
    );

    my @parsed_options_descriptions = map { Ghobe::Getopt::Mooptions::Option->new($_) } @$options_descriptions;
    my %opts = map { 
            $_->key => defined $_->default ? (ref($_->default) eq 'ARRAY' ? [] : ref($_->default) eq 'HASH' ? {} : '') : undef
        } @parsed_options_descriptions;
    $parser->getoptionsfromarray(\@args, \%opts, @$options_descriptions) or die "got errors parsing options";

    return ($class->new({'_options' => \@parsed_options_descriptions, '_opts' => \%opts}), \@args);
}

sub BUILD
{
    my ($self, $args) = @_;
    if (ref $args eq 'HASH' && ref $args->{'_options'} eq 'ARRAY')
    {
        for my $o (@{$args->{'_options'}})
        {
            my @attr_def = (is => 'ro');
            if (defined $o->default)
            {
                push(@attr_def, (lazy => 1, default => sub { ref($o->default) eq 'ARRAY' ? [] : ref($o->default) eq 'HASH' ? {} : '' }));
            }
            has($o->key => @attr_def);
        }
        map { $self->{$_} = $args->{'_opts'}->{$_} } keys %{$args->{'_opts'}} if ref $args->{'_opts'} eq 'HASH'
    }
}

1;

=head1 NAME

Ghobe::Getopt::Mooptions - wrapper around L<Getopt::Long> returning parsed options as an object

=head1 SYNOPSIS


    my ($opts, $args) = Ghobe::Getopt::Mooptions->parse([
        "length=i",
        "file=s",
        "verbose"
      ], @ARGV);
    
    if ($opts->verbose)
    {
      ...
    }
    do_something($opts->file, $opts->length);

=head1 METHODS

=head2 parse

    my ($opts, $args) = Ghobe::Getopt::Mooptions->parse(\@options_descriptions, @options);

C<options_descriptions> - array reference containing options descriptions suitable for L<Getopt::Long>.

C<options> - list of options to be parsed (e.g. C<@ARGV>)

Returns an options object and an array reference with all remaining parts from C<@options>.

=head1 AUTHOR

Christian Hesse, C<< <ghobe at ghobe.de> >>

=head1 SEE ALSO

L<Getopt::Long>

L<MooX::Options>

=cut