package Ghobe::Getopt::Mooptions::Option;
use Moo;

has 'def' => (is => 'ro');
has 'name' => (is => 'ro');
has 'key' => (is => 'ro');
has 'modifier' => (is => 'ro');
has 'assign' => (is => 'ro', default => '=');
has 'type' => (is => 'ro');
has 'desttype' => (is => 'ro');
has 'default' => (is => 'ro');
has 'repeat' => (is => 'ro');
has 'min_repeat' => (is => 'ro');
has 'max_repeat' => (is => 'ro');

my $re = qr/
		(?(DEFINE)
			(?<NAME> [\w\|-]+)
			(?<MODIFIER> [!+])
			(?<TYPE> [siof])
			(?<TYPENUM> [siof]|\d+)
			(?<DESTTYPE> [@%])
			(?<REPEAT> \d*(\s*,\s*\d*)?)
		)
		^
		(?<name>(?&NAME))(?<assign>=)(?<type>(?&TYPE))(?<desttype>(?&DESTTYPE))?(?:\{(?<repeat>(?&REPEAT))})? # <name> = type [ desttype ] [ repeat ]
		|
		(?<name>(?&NAME))(?<assign>:)(?<type>(?&TYPENUM))(?<desttype>(?&DESTTYPE))?(?:\{(?<repeat>(?&REPEAT))})? # <name> : (type|number) [ desttype ]
		|
		(?<name>(?&NAME))(?<modifier>(?&MODIFIER))? # <name>[!+]
	/x;
sub BUILDARGS
{
	my ($class, $arg) = @_;
	if (!ref($arg) && $arg =~ $re)
	{
		my $parsed = {'def' => $arg};
		for (qw(name modifier assign type desttype repeat))
		{
			$parsed->{$_} = $+{$_};
		}
		$parsed->{'key'} = (split(/\|/, $parsed->{'name'}))[0];
		if ($parsed->{'repeat'})
		{
			my @repeat = split(/\s*,\s*/, $parsed->{'repeat'}, -1);
			$parsed->{'min_repeat'} = $repeat[0];
			$parsed->{'max_repeat'} = $repeat[1];
		}
		if (defined $parsed->{'desttype'})
		{
			if ($parsed->{'desttype'} eq '@')
			{
				$parsed->{'default'} = [];
			}
			elsif ($parsed->{'desttype'} eq '%')
			{
				$parsed->{'default'} = {};
			}
		}
		elsif (defined $parsed->{'repeat'})
		{
			$parsed->{'desttype'} = '@';
			$parsed->{'default'} = [];
		}
		return $parsed;
	}
	return undef;
}

1;